class InitPopup
{
    constructor(indexNr)
    {

        // All Feed
        this.rssUrl = "https://www.upwork.com/ab/feed/topics/rss?securityToken=63ad19ab6d921bf91944759ee9153c588fa91216f8ad4668e4751ddf1638377c122d6a5849383e5c9615727819a1cfd0cac733e99d466d99e3d4246d3165a829&userUid=1157024983544967168&orgUid=1157024983549161473";
        //Java
        this.rssUrl2 = "https://www.upwork.com/ab/feed/topics/rss?securityToken=63ad19ab6d921bf91944759ee9153c588fa91216f8ad4668e4751ddf1638377c122d6a5849383e5c9615727819a1cfd0cac733e99d466d99e3d4246d3165a829&userUid=1157024983544967168&orgUid=1157024983549161473&topic=5518450";
        //API
        this.rssUrl3 = "https://www.upwork.com/ab/feed/topics/rss?securityToken=63ad19ab6d921bf91944759ee9153c588fa91216f8ad4668e4751ddf1638377c122d6a5849383e5c9615727819a1cfd0cac733e99d466d99e3d4246d3165a829&userUid=1157024983544967168&orgUid=1157024983549161473&topic=5520966";
        //Urgent
        this.rssUrl4 = "https://www.upwork.com/ab/feed/topics/rss?securityToken=63ad19ab6d921bf91944759ee9153c588fa91216f8ad4668e4751ddf1638377c122d6a5849383e5c9615727819a1cfd0cac733e99d466d99e3d4246d3165a829&userUid=1157024983544967168&orgUid=1157024983549161473&topic=5523442";
        //ASAP
        this.rssUrl5 = "https://www.upwork.com/ab/feed/topics/rss?securityToken=63ad19ab6d921bf91944759ee9153c588fa91216f8ad4668e4751ddf1638377c122d6a5849383e5c9615727819a1cfd0cac733e99d466d99e3d4246d3165a829&userUid=1157024983544967168&orgUid=1157024983549161473&topic=5573042";
        //Terraform
        this.rssUrl6 = "https://www.upwork.com/ab/feed/topics/rss?securityToken=63ad19ab6d921bf91944759ee9153c588fa91216f8ad4668e4751ddf1638377c122d6a5849383e5c9615727819a1cfd0cac733e99d466d99e3d4246d3165a829&userUid=1157024983544967168&orgUid=1157024983549161473&topic=5577399";
        
        this.urlToFetch = this.rssUrl;
        this.index = this.indexNr;
        if (indexNr == 2) {
            this.urlToFetch = this.rssUrl2;
        }
        else if (indexNr == 3) {
            this.urlToFetch = this.rssUrl3;
        }
        else if (indexNr == 4) {
            this.urlToFetch = this.rssUrl4;
        }
        else if (indexNr == 5) {
            this.urlToFetch = this.rssUrl5;
        }
        else if (indexNr == 6) {
            this.urlToFetch = this.rssUrl6;
        }
    }
    populatePopup(indexNr)
{
  fetch(this.urlToFetch).then((res) => {
    const names = ["PlaceHolder" , "All Feed", "Java", "API", "Urgent", "ASAP", "Terraform" ];
   // let indexNr = this.index;
  // chrome.extension.getBackgroundPage().console.log("PPindex nr is : ",  indexNr);

    res.text().then((plainxml) => {
     var domParser = new DOMParser();
     var xmlParsed = domParser.parseFromString(plainxml,'text/xml')
     xmlParsed.querySelectorAll('item').forEach((item) => {
         // Creating the render
         var h1 = document.createElement('h1');
         h1.textContent = item.querySelector('title').textContent;
       
         var publicationDate = document.createElement('span');
         var today = new Date();
         var date = new Date(item.querySelector('pubDate').textContent);


        publicationDate.textContent = diff_minutes(today,date).toString() + " minutes ago";
      
       
        var description = item.querySelector('description').textContent;
        var trimmedDesc = description.substring(0, 500);
        var p = document.createElement('p');
        p.innerHTML = trimmedDesc;

        var next = document.createElement('a');
        next.textContent = names[indexNr] + " - " + indexNr +  " > ";
        next.onclick = function () {

            if (indexNr == 1) {
                this.urlToFetch = this.rssUrl2;
                indexNr++;
            }
            else if (indexNr == 2) {
                this.urlToFetch = this.rssUrl3;
                indexNr++;
            }
            else if (indexNr == 3) {
                this.urlToFetch = this.rssUrl4;
                indexNr++;
            }
            else if (indexNr == 4) {
                this.urlToFetch = this.rssUrl5;
                indexNr++;
            }
            else if (indexNr == 5) {
                this.urlToFetch = this.rssUrl6;
                indexNr++;
            }
            else if (indexNr == 6) {
                this.urlToFetch = this.rssUrl;
                indexNr = 1;
            }
            document.getElementById('render-div').innerHTML = '';
            document.getElementById('render-div').textContent = '';

            new InitPopup(indexNr).populatePopup(indexNr);
        };

         var link = document.createElement('a');

         link.href = item.querySelector('link').textContent;
         link.appendChild(h1);
         link.appendChild(p);
         link.appendChild(publicationDate);
         link.onclick = function () {
            chrome.tabs.create({ active: true, url: item.querySelector('link').textContent });
        };
        document.getElementById('render-div').appendChild(next);
         document.getElementById('render-div').appendChild(link);
       
         })
      })
   });
}
}

function diff_minutes(dt2, dt1) 
 {

  var diff =(dt2.getTime() - dt1.getTime()) / 1000;
  diff /= 60;
  return Math.abs(Math.round(diff));
  
 }


chrome.browserAction.setBadgeText({ text: '' });
new InitPopup(1).populatePopup(1);